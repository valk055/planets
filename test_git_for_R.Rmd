---
title: "test_git_for_R"
author: "Laura Valk"
date: "2023-04-28"
output: html_document
---

```{r}
# load libraries
library(tidyverse)

```

```{r}
mars <- read.delim("mars.txt")
venus <- c("test", "test2", "test3")

venus_df <- as_tibble(venus)

write_delim(venus_df, "venus_2.txt", delim = "/t")

test3 <- as_tibble(c("bla2", "bla2", "bla4"))


write_delim(test3, "saturnus.txt", delim = "/t")

```